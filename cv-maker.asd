;;;; cv-maker.asd

(asdf:defsystem #:cv-maker
  :serial t
  :depends-on (#:cl-who
               #:cl-ppcre
               #:iterate)
  :components ((:file "package")
	       (:file "file-parser")
               (:file "cv-maker")
	       (:static-file "cv-source")
	       (:static-file "preamble")))

