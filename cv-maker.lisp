;;;; cv-maker.lisp

(in-package #:cv-maker)

;;; "cv-maker" goes here. Hacks and glory await!

(defmacro with-html (&body body)
  "Thin wrapper around with-html-output-to-string"
  `(cl-who:with-html-output-to-string
    (*standard-output* nil :indent t)
     ,@body))

(defun parse-text-field (text)
  "Takes a string and returns an alist of fields. Fields are separated
with | and all except the first one had an identifier followed by a :"
  (let ((result nil)
	(subfield-list (cl-ppcre:split "\\|" text)))
    (flet ((parse-subfield (subfield)
	     (cons (read-from-string (first subfield))
		   (second subfield))))
      (dolist (subfield (cdr subfield-list))
	(push (parse-subfield (cl-ppcre:split ":" subfield)) result))
      (push (cons 'text (car subfield-list)) result))))

(defun subfield->html (subfield)
  "Prints the subfields in an alist to html based on its type"
  (let ((content (cdr subfield)))
    (case (car subfield)
      (text content)
      (slides (with-html (:a :href (cl-who:htm content) "[pdf slides]")))
      (handout (with-html (:a :href (cl-who:htm content) "[pdf handout]")))
      (ss (with-html (:span :class "clickme"
			     (:span :class "clickme-link" "Click")
			     " to view presentation")
	              (:div :class "ss-embed"
                           (:iframe :src (with-output-to-string (s)
					   (format s "http://slideshare.net/slideshow/embed_code/~a/" content))))))
      (other (with-html (str content))))))

(defun text->html (subfield-list)
  "Pulls together the subfields in the right order"
  (with-output-to-string (s)
    (format s "~{~a~^ ~}" (mapcar #'subfield->html
				  (mapcar (lambda (key)
					    (assoc key subfield-list))
					  '(text handout slides other ss))))))

(defun entry->html (entry current-heading)
  "Prints out the heading and the html of the entry"
  (let ((heading (heading entry))
	(text (text entry)))
    (if (equal heading current-heading)
	(with-html
	  (:div :id "cv-item" (str text)))
	(with-html
	  (:div :id "cv-title" (str heading))
          (:div :id "cv-item" (str text))))))

(defun entrylist->html (entry-list)
  "Prints out a whole list of entries, taking care not to print
repeating headings"
  (labels ((do-entry-list (entry-list current-heading html-list)
	     (if entry-list
		 (progn (push (entry->html (car entry-list) current-heading) html-list)
			(do-entry-list (cdr entry-list) (heading (car entry-list)) html-list))
		 (nreverse html-list))))
    (apply #'concatenate 'string (do-entry-list entry-list nil nil))))

(defun section->html (section)
  "Prints out a section or subsection"
  (let ((heading (if (parent section)
		     (with-html (:h3 (str (heading section))))
		     (with-html (:h2 (str (heading section)))))))
  (if (entries section)
      (concatenate 'string heading (entrylist->html (entries section)))
      (apply #'concatenate 'string heading (mapcar #'section->html (sections section))))))

(defun cv->html (cv)
  "Prints out the whole shebang"
  (apply #'concatenate 'string (mapcar #'section->html (sections cv))))