;;;; file-parser.lisp

(in-package #:cv-maker)

(defparameter *trim-bag* '(#\# #\Space #\Tab))

;; Data structures. Of course this could live in an alist or a bunch
;; of structs, but this is just easier

(defclass cv ()
  ((section-list
    :initarg :sections
    :initform nil
    :accessor sections
    :documentation "The whole shebang")))

(defclass section (cv)
  ((heading
    :initarg :heading
    :accessor heading
    :documentation "The title")
   (entries
    :initarg :entries
    :accessor entries
    :initform nil
    :documentation "Entries if there are no subsections")
   (parent
    :initarg :parent
    :accessor parent
    :initform nil
    :documentation "Tracks whether a parent section exists")))

(defclass entry ()
  ((heading
    :initarg :heading
    :accessor heading
    :documentation "Year or other info")
   (text
    :initarg :text
    :accessor text
    :documentation "The entry's content")))

(defgeneric cv-print (object)
  (:documentation "Pretty-print to the REPL"))

(defmethod cv-print ((object entry))
  (format t "Entry: ~a~10t~%~5t~a~%" (heading object) (text object)))

(defmethod cv-print ((object section))
  (format t "Section: ~a~%" (heading object))
    (dolist (x (if (sections object)
		  (sections object)
		  (entries object)))
      (cv-print x)))
  
(defmethod cv-print ((object cv))
  (dolist (section (sections object))
    (cv-print section)))

;;;; Parse the source file into a CV object

(defmacro rev-slot (&body body)
  "Just some syntactic sugar to reverse lists in object slots"
  `(setf ,@body  (reverse ,@body)))

(defun parse-cv-source-file (&optional (file "/home/pasha/Dropbox/Lisp/cv-maker/cv-source"))
  "Parse the source file. At this point it's ugly and relies on side
   effects. FIXME: rewrite"
  (let ((current-section nil)
	(current-subsection nil)
	(current-entry-heading nil)
	(cv (make-instance 'cv)))
    (flet ((parse-line (line)
	     (cond
	       ((search "###" line) (progn (setq current-entry-heading (string-trim *trim-bag* line))))
	       ((search "##" line) (progn (when current-subsection
					    (rev-slot (entries current-subsection))
					    (push current-subsection (sections current-section)))
					  (setq current-subsection (make-instance 'section
										  :heading (string-trim *trim-bag* line)
										  :parent t))))
	       ((search "#" line) (progn (when current-section
					   (when (entries current-section)
					     (rev-slot (entries current-section)))
					   (when (sections current-section)
					     (rev-slot (sections current-section)))
					   (push current-section (sections cv)))
					 (setq current-section (make-instance 'section
									      :heading (string-trim *trim-bag* line)))
					 (setq current-subsection nil)))
	       (t (let ((new-entry (make-instance 'entry
						  :heading current-entry-heading
						  :text (string-trim *trim-bag* line))))
		    (if current-subsection
			(push new-entry (entries current-subsection))
			(push new-entry (entries current-section))))))))
      (iter (for line in-file file using #'read-line)
	    (when (> (length line) 0)
	      (parse-line line))
	    (finally (when current-subsection  ;; OK, this really sucks
		       (rev-slot (entries current-subsection))
		       (push current-subsection (sections current-section)))
		     (when current-section
		       (when (entries current-section)
			 (rev-slot (entries current-section)))
		       (when (sections current-section)
			 (rev-slot (sections current-section)))
		       (push current-section (sections cv)))
		     (rev-slot (sections cv))))
      cv)))

(defparameter *cv* (parse-cv-source-file))